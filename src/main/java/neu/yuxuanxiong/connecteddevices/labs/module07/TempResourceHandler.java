package neu.yuxuanxiong.connecteddevices.labs.module07;

import java.util.logging.Logger;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.server.resources.CoapExchange;

import com.google.gson.Gson;

import neu.yuxuanxiong.connecteddevices.labs.common.DataUtil;
import neu.yuxuanxiong.connecteddevices.labs.common.SensorData;

public class TempResourceHandler extends CoapResource {
	
	// initialize variables , instances, new objects 
	
	private static final Logger _Logger = Logger.getLogger(TempResourceHandler.class.getName());
	private SensorData sd = new SensorData();
	private Gson gson = new Gson();
	
	// default constructor
	public TempResourceHandler() {
		super("temp");
	}
	
	// constructor with params name
	public TempResourceHandler(String name) {
		super(name);
	}
	
	// constructor with params name and visible
	public TempResourceHandler(String name, boolean visible) {
		super(name,visible);
	}
	
	// method of handle GET request
	public void handleGET(CoapExchange ce) {
		ce.respond(ResponseCode.CREATED, "GET worked!");    // response CREATED code and message of "GET worked"
		_Logger.info("Received GET request from client...");  // log the info that received GET on the console
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.californium.core.CoapResource#handlePOST(org.eclipse.californium.core.server.resources.CoapExchange)
	 * method of handle POST request, get JSON format SensorData from client
	 * transfer JSON into sensorData instance, show on the console
	 * transfer SensorData instance into JSON, show on the console
	 */
	
	public void handlePOST(CoapExchange ce) {
		
		_Logger.info("The SensorData in JSON format POST by the client is:");
		String response = ce.getRequestText();					// get the request text from client
		_Logger.info(response);									// log the request JSON format of sensorData on the console
		DataUtil du = new DataUtil();
		sd = du.jsonToSensorData(response);						// transfer the JSON data into SensorData instance
		_Logger.info("Transfer JSON into SensorData instance: ");
		_Logger.info(sd.output());
		_Logger.info("Transfer SensorData instance into JSON format: ");
		String payload = gson.toJson(sd);						// transfer the SensorData instance into JSON
		_Logger.info(payload);
		ce.respond(ResponseCode.CONTENT, payload);				// respond the CONTENT response code and JSON format sensorData to client
		
		_Logger.info("Received POST request from client...");
	}
	
	// method of handle PUT request
	public void handlePUT(CoapExchange ce) {
		
		ce.respond(ResponseCode.VALID, "PUT worked!");			// respond VALID code and "PUT worked " to client
		
		_Logger.info("Received PUT request from client...");
	}
	
	// method of handle DELETE request
	public void handleDELETE(CoapExchange ce) {
		ce.respond(ResponseCode.DELETED, "DELETE worked!");		// respond DELETED code and "DELETE worked!" to client
		
		_Logger.info("Received DELETE request from client...");
	}
}

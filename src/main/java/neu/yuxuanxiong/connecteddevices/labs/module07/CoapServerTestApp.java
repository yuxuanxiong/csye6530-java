package neu.yuxuanxiong.connecteddevices.labs.module07;

public class CoapServerTestApp {
	private static CoapServerTestApp _App;
	
	public static void main(String[] args) {
		
		_App = new CoapServerTestApp();
		
		try {
			_App.start();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	// private var's
	
	private CoapServerConnector _coapServer;
	
	// constructor
	
	public CoapServerTestApp() {
		super();
	}
	
	//public methods of start coap server
	
	public void start() {
		_coapServer = new CoapServerConnector();
		_coapServer.start();
	}

}

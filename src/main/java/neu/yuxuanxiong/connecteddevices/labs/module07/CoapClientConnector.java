package neu.yuxuanxiong.connecteddevices.labs.module07;

import com.google.gson.Gson;
import com.labbenchstudios.edu.connecteddevices.common.ConfigConst;

import neu.yuxuanxiong.connecteddevices.labs.common.SensorData;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.WebLink;
import org.eclipse.californium.core.coap.MediaTypeRegistry;

public class CoapClientConnector {
	// initial variables, objects 
	String _host;
	String _protocol;
	int _port;
	String _serverAddr;
	private static final Logger _Logger = Logger.getLogger(CoapClientConnector.class.getName());
	private boolean _isInitialized ;
	CoapClient _clientConn;
	
	// default constructor, set coap server
	public CoapClientConnector() {
		this(ConfigConst.DEFAULT_COAP_SERVER, false);
	}
	
	// constructor, initialize coap protocol ,port server
	public CoapClientConnector(String host, boolean isSecure) {
		super();
		
		if(isSecure) {
			_protocol = ConfigConst.SECURE_COAP_PROTOCOL;      // set coap protocol    
			_port     = ConfigConst.SECURE_COAP_PORT;         // set coap port
		}else {
			_protocol = ConfigConst.DEFAULT_COAP_PROTOCOL;
			_port     = ConfigConst.DEFAULT_COAP_PORT;
		}
		
		if(host !=null && host.trim().length() >0) {
			_host = host;
		} else {
			_host = ConfigConst.DEFAULT_COAP_SERVER;      // set address of host
		}
		
		// NOTE: URL does not have a protocol handler for "coap",
		// so we need to construct the URL manually
		
		_serverAddr = _protocol + "://" + _host + ":" + _port;
		_Logger.info("Using URL for server conn:" + _serverAddr);
	}
	
	
	// runTests method to transfer sensorData instance into JSON format
	// send PING, GET, PUT, POST, DELETE requests
	
	public void runTests(String resourceName) {
		try {
			_isInitialized = false;
			initClient(resourceName);						// initial client
			_Logger.info("Current URI: " + getCurrentUri());
			SensorData sd = new SensorData();				// new sensordata object
			Gson gson = new Gson();
			String payload = gson.toJson(sd);              // transfer SensorData instance into JSON format
			System.out.println(payload);
			pingServer();
			discoverResources();
	        sendGetRequest();
	        sendGetRequest(true);
	        sendPostRequest(payload, false);
	        sendPostRequest(payload, true);
	        sendPutRequest(payload, false);
	        sendPutRequest(payload, true);
	        sendDeleteRequest();
	    } catch (Exception e) {
	          _Logger.log(Level.SEVERE, "Failed to issue request to CoAP server.", e);
		}
	}
	
	// discover resource, test the connection
	public void discoverResources() {
		_Logger.info("Issuing discover...");
		
		initClient();
		
		Set<WebLink> wlSet = _clientConn.discover();
		
		if(wlSet != null) {
			for(WebLink wl : wlSet) {
				_Logger.info(" --> WebLink:" + wl.getURI());
			}
		}
	}
	
	// method of get current URI
	public String getCurrentUri()
	{
	    return (_clientConn != null ? _clientConn.getURI() : _serverAddr);
	}

	// method of ping server
	public void pingServer()
	{
	    _Logger.info("Sending ping...");
	    initClient();
	    if(_clientConn.ping()) {       				// if ping() return true means ping successful
	    	_Logger.info("Ping successful!");
	    }

	  }
	
	// method to send DELETE request
	public void sendDeleteRequest()
	{
	    initClient();
	    handleDeleteRequest();
	}
	
	// method to send DELETE request through resourceName
	public void sendDeleteRequest(String resourceName)
	{
	    _isInitialized = false;
	    initClient(resourceName);
	    handleDeleteRequest();
	}
	
	// method to send GET request
	public void sendGetRequest()
	{
	    initClient();
	    handleGetRequest(false);
	}
	
	// method to send GET request through resourceName
	public void sendGetRequest(String resourceName)
	{
	    _isInitialized = false;
	    initClient(resourceName);
	    handleGetRequest(false);
	}
	
	// method of send GET request by using NON
	public void sendGetRequest(boolean useNON)
	{
	    initClient();
	    handleGetRequest(useNON);
	}
	
	// method of send GET request through resourceName and decide to use NON or not
	public void sendGetRequest(String resourceName, boolean useNON)
	{
	    _isInitialized = false;
	    initClient(resourceName);
	    sendGetRequest(useNON);
	}
	
	// method of send POST request, send payload and decide to useCON or not
	public void sendPostRequest(String payload, boolean useCON)
	{
	    initClient();
	    handlePostRequest(payload, useCON);
	}
	
	// method of send POST request through resourceName, send payload and decide to useCON or not
	public void sendPostRequest(String resourceName, String payload, boolean useCON)
	{
	    _isInitialized = false;
	    initClient(resourceName);
	    handlePostRequest(payload, useCON);
	}
	
	// method of send PUT request, send payload and decide to useCON or not
	public void sendPutRequest(String payload, boolean useCON)
	{
	    initClient();
	    handlePutRequest(payload, useCON);
	}
	
	// method of send PUT request through resourceName, send payload and decide to useCON or not
	public void sendPutRequest(String resourceName, String payload, boolean useCON)
	{
	    _isInitialized = false;
	    initClient(resourceName);
	    handlePutRequest(payload, useCON);
	}
	
	// private methods of handle DELETE request
	private void handleDeleteRequest()
	{
	    _Logger.info("Sending DELETE...");
	    CoapResponse response = null;										// create a new CoapResponse object
	    response = _clientConn.delete();
	    if(response != null) {
	    	_Logger.info("Response: " + response.isSuccess() + " - " + response.getOptions() + " - " + response.getCode()); // log the message to show response information
	    }else {
	    	_Logger.warning("No response received. ");
	    }
	}
	
	// private methods to handle GET request
	private void handleGetRequest(boolean useNON)
	{
		_Logger.info("Sending GET...");
		CoapResponse response = null;
	    if (useNON) {
	          _clientConn.useNONs().useEarlyNegotiation(32).get();    // if useNON is true, use NON to send get request
	}
	    response = _clientConn.get();
	    if(response != null) {
	    	_Logger.info(
                    "Response: " + response.isSuccess() + " - " + response.getOptions() + " - " + response.getCode());  // log the message to show response information
	          
	    } else {
	    	_Logger.warning("No response received.");
	    }
	    
	}
	
	// private methods to handle PUT request
	private void handlePutRequest(String payload, boolean useCON)
	{
	    _Logger.info("Sending PUT...");
	    CoapResponse response = null;
	    if (useCON) {
	          _clientConn.useCONs().useEarlyNegotiation(32).get();
	}
	    response = _clientConn.put(payload, MediaTypeRegistry.TEXT_PLAIN);
	    if (response != null) {
	          _Logger.info(
	                        "Response: " + response.isSuccess() + " - " + response.getOptions() + " - " + response.getCode());  // log the message to show response information
	          
	} else {
		_Logger.warning("No response received.");
		} 
	    
	}
	
	// private methods to handle POST request
	private void handlePostRequest(String payload, boolean useCON)
	{
	    _Logger.info("Sending POST...");
	    CoapResponse response = null;
	    if (useCON) {
	        _clientConn.useCONs().useEarlyNegotiation(32).get();
	 }
	 response = _clientConn.post(payload, MediaTypeRegistry.TEXT_PLAIN);										// send POST request with payload
	 if (response != null) {
	        _Logger.info(
	                     "Response: " + response.isSuccess() + " - " + response.getOptions() + " - " + response.getCode());  // log the message to show response information
	        _Logger.warning("No response received.");
	 } else {
		 _Logger.warning("No response received.");
	 } 
	 }
	
	// method of initial client without params
	private void initClient() {
		initClient(null);
		
	}
	
	// method of initial client with resourceName
	public void initClient(String resourceName) {
		
		if(_isInitialized) {						// if the client is initialized, so return
			return;
		}
		if(_clientConn != null) {
			_clientConn.shutdown();					// if connection have been established, shutdown the connection
			_clientConn = null;
		}
		try {
			if(resourceName != null) {
				_serverAddr += "/" + resourceName;
			}
			
			_clientConn = new CoapClient(_serverAddr);
			
			_Logger.info("Created client connection to server / resource: " + _serverAddr); 	// if connection has been established, log the message 
			
		}catch(Exception e) {
			_Logger.log(Level.SEVERE, "Failed to connect to broker: " + getCurrentUri(), e);
		}
	}

}

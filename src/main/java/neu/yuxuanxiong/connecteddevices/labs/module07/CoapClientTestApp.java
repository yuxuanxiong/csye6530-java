package neu.yuxuanxiong.connecteddevices.labs.module07;

import java.util.logging.Logger;

public class CoapClientTestApp {
	//static
	
	private static final Logger _Logger = 
			Logger.getLogger(CoapClientTestApp.class.getName());
	
	private static CoapClientTestApp _App;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		_App = new CoapClientTestApp();      // new CoapClientTestApp object
		
		try {
			_App.start();					// run start method
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	// private var's
	
	private CoapClientConnector _coapClient;
	
	// constructors
	
	/**
	 * 
	 */
	public CoapClientTestApp() {
		super();
	}
	
	// public methods
	
	/**
	 *  Connect to the CoAP server
	 */
	public void start() {
		_coapClient = new CoapClientConnector(); 			// new CoapClientConnector object
		_coapClient.runTests("temp");
	}

}

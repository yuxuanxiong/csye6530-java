package neu.yuxuanxiong.connecteddevices.labs.module06;

// import statements

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import com.labbenchstudios.edu.connecteddevices.common.ConfigConst;

public class MqttClientConnector implements MqttCallback {
 
 private static final Logger _Logger = Logger.getLogger(MqttClientConnector.class.getName());
 private String _MqttClientID;
 private String _brokerAddress;
 private MqttClient _Client;
 private String _protocol = ConfigConst.DEFAULT_MQTT_PROTOCOL;
 private String _host = ConfigConst.DEFAULT_MQTT_SERVER;
 private int _port = ConfigConst.DEFAULT_MQTT_PORT;
 private String _userName;
 private String _pemFileName;
 private boolean _isSecureConn = true;
 
 
 
 public MqttClientConnector() {
  this(null, false);
 }
 
 public MqttClientConnector(String host, boolean isSecure) {
  super();
   
  //  'isSecure' ignored for now
  if (host != null && host.trim().length() > 0) {
   _host = host;
  }
  
  // NOTE: URL does not have a protocol handler for "tcp", construct the URL manually  
  _MqttClientID = MqttClient.generateClientId();
  _Logger.info("Using client ID for broker conn: " + _MqttClientID);
  _brokerAddress = _protocol + "://" + _host + ":" + _port;
  _Logger.info("Using URL for broker conn: " +  _brokerAddress);
 }
 
//constructor
/**
* Constructor.
*
* @param host
* @param userName
* @param pemFileName The name of the certificate file to use. If null / invalid, ignored.
*/
 public MqttClientConnector(String host, String userName, String pemFileName) {
	 super();
	 
	 if(host != null && host.trim().length() > 0) {
		 _host = host;
	 }
	 
	 if(userName !=null && userName.trim().length() > 0) {
		 _userName = userName;
	 }
	 
	 if(pemFileName != null) {
		 File file = new File(pemFileName);
		 
		 if(file.exists()) {
			 _protocol = "ssl";
			 _port = 8883;
			 _pemFileName = pemFileName;
			 _isSecureConn = true;
			 
			 _Logger.info("PEM file valid. Using secure connection: " + _pemFileName);
			 
		 }else {
			 _Logger.info("PEM file invalid. Using insecure connection: " + pemFileName);
		 }
	 }
	 
	 _MqttClientID = MqttClient.generateClientId();
	 _brokerAddress = _protocol + "://" + _host + ":" + _port;
	 _Logger.info("MqttClientID :" + _MqttClientID);
	 _Logger.info("Using URL for broker conn: " + _brokerAddress);
 }
 
 public void connect() {
	 if (_Client == null) {
		 MemoryPersistence persistence = new MemoryPersistence();
		 try {
			 _Client = new MqttClient(_brokerAddress, _MqttClientID, persistence);
			 MqttConnectOptions cOptions = new MqttConnectOptions();
			 cOptions.setCleanSession(true);
			 _Logger.info("Connecting to the broker: " + _brokerAddress);
			 _Client.setCallback(this);
			 _Client.connect(cOptions);
			 _Logger.info("Connected to the broker: " + _brokerAddress);
		 } catch (MqttException e) {
			 _Logger.log(Level.SEVERE, "Failed!! to connect to the broker: " + _brokerAddress, e);
			 }
		 }
	 }
 
 public void disconnect() {
	 try {
		 _Client.disconnect();
		 _Logger.info("Disconnected from the broker: " + _brokerAddress);
	 } catch (Exception e) {
		 _Logger.log(Level.SEVERE, "Failed!!! to disconnect from the broker: " +  _brokerAddress, e);
		 }
	 }
 
 public boolean publishMessage(String topic, int qosLevel, byte[] payload) {
	 boolean success = false;
	 try {
		 _Logger.info("Publishing message to topic: " + topic);
   
		 //create a new MqttMessage, pass 'payload' to the constructor
		 MqttMessage msg = new MqttMessage(payload);
  
		 //set the QoS to qosLevel
		 msg.setQos(qosLevel);
   
		 //call 'publish' on the MQTT client, passing the 'topic' and MqttMessage
		 msg.setRetained(true);
		 _Client.publish(topic, msg);
		 success = true;
		 _Logger.info("Published message to topic: " + topic);
		 } catch (Exception e) {
		 _Logger.log(Level.SEVERE, "Failed!!! to publish MQTT message: " + e.getMessage());
		 }
	 return success;
 	}
 
 // subscribe to all
 public boolean subscribeToAll() {
	 try {

		 _Client.subscribe("$SYS/#");
		 _Logger.log(Level.INFO, "Subscribe to all successfully.");
		 return true;
	 } catch (MqttException e) {
		 _Logger.log(Level.WARNING, "Failed!! to subscribe to all topics.", e);
	 }
	 return false;
 	}
 
 // subscribe to topic
 public boolean subscribeToTopic(String topic) {
	 try {
		 _Client.subscribe(topic);
		 _Logger.log(Level.INFO, "Subscribe to Topic successfully.");
		 return true;
	 } catch (MqttException e) {
		 _Logger.log(Level.WARNING, "Failed!! to subscribe to Topic topics.", e);
	 }
	 return false;
 	}
 
 // connection lost
 public void connectionLost(Throwable t) {
	 _Logger.log(Level.WARNING, "Connection lost.....", t);
 	}

 //deliveryComplete
 public void deliveryssComplete(IMqttDeliveryToken token) {
	 try {
		 _Logger.info("Delivery complete: " + token.getMessageId() + " - " + token.getResponse() + " - "
				 + token.getMessage());
	 } catch (Exception e) {
		 _Logger.log(Level.SEVERE, "Failed!! to retrieve message from token.", e);
	 }
 	}
 
 // when message published then log message on the console
 public void messageArrived(String data, MqttMessage msg) throws Exception {
	
	 _Logger.info("Message arrived: " + data + ", " + msg.getId() + ", " + msg.toString());
	 System.out.println("Payload Received: " + msg.toString());
 	}

 public void deliveryComplete(IMqttDeliveryToken token) {
	 // TODO Auto-generated method stub
  
 	}
 
 /*
  * secure connection method
  * use readCerficate() to load the Ubidots root certificate
  */
 public void initSecureConnection(MqttConnectOptions connOpts) {
	 try {
		 _Logger.info("Configuring TLS...");
		 
		 SSLContext sslContext = SSLContext.getInstance("SSL");
		 KeyStore keyStore = readCerficate();
		 
		 TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		 trustManagerFactory.init(keyStore);
		 sslContext.init(null, trustManagerFactory.getTrustManagers(), new SecureRandom());

		 connOpts.setSocketFactory(sslContext.getSocketFactory());
		 
	 }catch(Exception e) {
		 _Logger.log(Level.SEVERE, "Failed to initialize secure MQTT connection.", e);
	 }
 }
 
 /*
  * readCerficate(0) method to load the Ubidots root certificates
  */
 private KeyStore readCerficate() throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException{
	 KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
	 FileInputStream fis = new FileInputStream(_pemFileName);
	 BufferedInputStream bis = new BufferedInputStream(fis);
	 CertificateFactory cf = CertificateFactory.getInstance("X.509");
	 
	 ks.load(null);
	 
	 while(bis.available() > 0) {
		 Certificate cert = cf.generateCertificate(bis);
		 ks.setCertificateEntry("adk_store" + bis.available(),cert); 
	 }
	 
	 return ks;
	 
 }
 


}
package neu.yuxuanxiong.connecteddevices.labs.module06;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.Gson;

import neu.yuxuanxiong.connecteddevices.labs.common.SensorData;


public class MqttPubClientTestApp {
 
 private static final Logger _Logger = Logger.getLogger(MqttPubClientTestApp.class.getName());
 private static MqttPubClientTestApp _App;
 private MqttClientConnector _Client;
 
public MqttPubClientTestApp() {
  // TODO Auto-generated constructor stub
 }
 
// the main method, start the app
 public static void main(String[] args) {
  _App = new MqttPubClientTestApp();
  try {
   _App.start();
  } catch (Exception e) {
   _Logger.log(Level.WARNING, "Failed!! to start _App.", e);
  }
 }
 
 // the start method to establish connection and set topic, transfer sensorData instance to JSON then publish to broker
 public void start() {
  //Create a new MqttClient:_Client and connection
  _Client = new MqttClientConnector();
  SensorData _SensorData = new SensorData();
  _Client.connect();
  
  //Set topic 
  String topic = "Test";
  
  // Set the payload for publishing...
  
  Gson gson = new Gson();
  System.out.println("Print sensorData:");
  System.out.println(_SensorData.output());
  String json = gson.toJson(_SensorData);
  System.out.println("Transfer sensorData to Json");
  System.out.println(json);  

  _Client.publishMessage(topic, 2, json.getBytes());      //publish payload with the topic in Qoslevel 2
  _Client.disconnect();
 }
 
 
}
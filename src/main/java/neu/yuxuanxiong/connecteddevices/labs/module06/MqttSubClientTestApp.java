package neu.yuxuanxiong.connecteddevices.labs.module06;

import java.util.logging.Level;
import java.util.logging.Logger;

public class MqttSubClientTestApp {
	 private static MqttSubClientTestApp _App;
	 private static final Logger _Logger = Logger.getLogger(MqttSubClientTestApp.class.getName());
	 private static MqttClientConnector mqttClient;

	public static void main(String[] args) {
		  _App = new MqttSubClientTestApp();
		  try {
		   _App.start();
		  } catch (Exception e) {
		   _Logger.log(Level.WARNING, "Failed!! to start _App.", e);
		  }
		 }
	public void start() {
		String topic = "Test";
		mqttClient.connect();
		mqttClient.subscribeToTopic(topic);
		
	}

}

package neu.yuxuanxiong.connecteddevices.labs.module08;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.ubidots.*;

public class TempSensorCloudPublisherApp 
{
	private String _userName = "A1E-bd039418bfcb2f221b1c7415e4038c8e4ae8";
	private String _id = "5c9a9c9c591636530f6571b0";
	private static final Logger _Logger = Logger.getLogger(TempSensorCloudPublisherApp.class.getName());
	private static TempSensorCloudPublisherApp _App;
	
	/**
	 * Default constructor
	 */
	public TempSensorCloudPublisherApp() 
	{
		super();
	}

	/**
	 * Start the application, 
	 * connect to the Apiclient and
	 * publish payload to the topic
	 */
	public void start() 
	{
		//Create a new ApiClient:_Client and connection
		ApiClient api = new ApiClient(_userName);
		DataSource dataSource = api.getDataSource(_id);
		Variable[] variable = dataSource.getVariables();
        try
        {
        	while(true)
			{
				Random r = new Random();
				float temp  = r.nextFloat()*30;
				System.out.println("Payload: " + temp);
				variable[1].saveValue(temp);
				Thread.sleep(60*1000);
			}
        }
        catch(Exception ex)
        {  
            ex.printStackTrace();  
        }  		
	}
	
	/**
	 * Main function
	 * @param args: command line arguments
	 */
	public static void main(String[] args) 
	{
		_App = new TempSensorCloudPublisherApp();
		try 
		{
			_App.start();
		} 
		catch (Exception e) 
		{
			_Logger.log(Level.WARNING, "Failed!! to start _App.", e);
		}
	}
}
package neu.yuxuanxiong.connecteddevices.labs.module08;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import neu.yuxuanxiong.connecteddevices.labs.module06.MqttClientConnector;

public class TempSensorPublisherApp {
	
	private static final Logger _Logger = Logger.getLogger(TempSensorPublisherApp.class.getName());
	private static TempSensorPublisherApp _App;
	private MqttClientConnector _Client;
	private String _host = null;
	private String _pemFileName = "/Users/yuxuanxiong/eclipse-workspace/connected-devices-java/src/main/java/neu/yuxuanxiong/connecteddevices/labs/common/ubidots_cert.pem";
	private String _userName = "A1E-q4QvqPSBqBpZ2y0tsasiBhCIsIoXou";
 
	/* Main function
	 * start the app
	 */
	public static void main(String[] args) {
		_App = new TempSensorPublisherApp();
	   try {
		   _App.start();
	   } catch (Exception e) {
	    _Logger.log(Level.WARNING, "Failed!! to start _App.", e);
	   }
	  
	 }
	 
	 /*
	  * the start method to establish connection and set topic
	  * generate and publish payload every minute
	  */
	 public void start() {
	  //Create a new MqttClient:_Client and connection
	  _Client = new MqttClientConnector(_host,_userName,_pemFileName);
	  _Client.connect();
	  
	  //Set topic 
	  String topic = "/v1.6/devices/xyxdevice/tempsensor";
	  
	  //Set the payload for publishing...
	  try {
	        while (true) {
	        	Random r = new Random();
	        	String payload = String.valueOf(r.nextFloat()*30);
	      	  	System.out.println("payload :" + payload);  
	        	
	      	  	_Client.publishMessage(topic, 0, payload.getBytes());      //publish payload with the topic in Qoslevel 2
	            Thread.sleep(60 * 1000);
	        }
	    } catch (InterruptedException e) {
	        e.printStackTrace();
	    }
	  _Client.disconnect();
	  
	  
	 }
	 
}

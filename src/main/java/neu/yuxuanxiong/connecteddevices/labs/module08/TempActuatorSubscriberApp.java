package neu.yuxuanxiong.connecteddevices.labs.module08;

import java.util.logging.Level;
import java.util.logging.Logger;

import neu.yuxuanxiong.connecteddevices.labs.module06.MqttClientConnector;

public class TempActuatorSubscriberApp {
	
	private static final Logger _Logger = Logger.getLogger(TempActuatorSubscriberApp.class.getName());
	 private static TempActuatorSubscriberApp _App;
	 private MqttClientConnector _Client;
	 private String _host = null;
	 private String _userName = "A1E-q4QvqPSBqBpZ2y0tsasiBhCIsIoXou";
	 private String _pemFileName = "/Users/yuxuanxiong/eclipse-workspace/connected-devices-java/src/main/java/neu/yuxuanxiong/connecteddevices/labs/common/ubidots_cert.pem";

	/* main method
	 * start the app
	 */
	 public static void main(String[] args) {
	  _App = new TempActuatorSubscriberApp();
	  try {
		  
	   _App.start();
	  
	  } catch (Exception e) {
	   _Logger.log(Level.WARNING, "Failed!! to start _App.", e);
	  }
	  
	 }
	 
	 /* start method to establish mqtt secure connection 
	  * set topic
	  * publish payload to broker
	  */
	 public void start() {
	  //Create a new MqttClient:_Client and connection
	  _Client = new MqttClientConnector(_host,_userName,_pemFileName);
	  _Client.connect();
	  String topic = "/v1.6/devices/xyxdevice/tempactuator";
	  _Client.subscribeToTopic(topic);
	 }
}

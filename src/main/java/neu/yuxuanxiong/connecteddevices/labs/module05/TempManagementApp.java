package neu.yuxuanxiong.connecteddevices.labs.module05;

import java.io.FileNotFoundException;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import neu.yuxuanxiong.connecteddevices.labs.common.DataUtil;
import neu.yuxuanxiong.connecteddevices.labs.common.SensorData;

public class TempManagementApp {
	
	// create DataUtil object and SensorData object
	// start to read JSon file and convert JSon string to sensorData instance
	// then convert senseData to JSon
	
	public static void main(String[] args) throws JsonIOException, JsonSyntaxException, FileNotFoundException {
		DataUtil data = new DataUtil();
		System.out.println("Convert SensorData from JSon: ");
		SensorData sensordata = data.fromJson();
		System.out.println("Convert SensorData to JSon: ");
		data.sensorDataToJson(sensordata);
		
	}

}

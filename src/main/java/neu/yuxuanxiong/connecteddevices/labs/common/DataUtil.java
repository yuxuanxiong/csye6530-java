package neu.yuxuanxiong.connecteddevices.labs.common;

import java.io.FileReader;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import java.io.FileNotFoundException;

public class DataUtil {
	
	// fromJson method is to read json string from json file
	// convert it to SensorData instances
	// print all data after converting
	// return the object of SensorData
	public SensorData fromJson() throws JsonIOException, JsonSyntaxException, FileNotFoundException {
		SensorData sensordata = new SensorData();
		// read file by fileReader
		JsonObject jbt = new JsonParser().parse(new FileReader("/Users/yuxuanxiong/git/iot-gateway-python/data/data.json")).getAsJsonObject();
		
		sensordata.name = jbt.get("name").getAsString();
		sensordata.timeStamp = jbt.get("timeStamp").getAsString();
		sensordata.avgValue = jbt.get("avgValue").getAsFloat();
		sensordata.minValue = jbt.get("minValue").getAsFloat();
		sensordata.maxValue = jbt.get("maxValue").getAsFloat();
		sensordata.curValue = jbt.get("curValue").getAsFloat();
		sensordata.totValue = jbt.get("totValue").getAsFloat();
		sensordata.sampleCount = jbt.get("sampleCount").getAsInt();
		
		System.out.println("Name: " + sensordata.name);
		System.out.println("Timestamp: " + sensordata.timeStamp);
		System.out.println("Average : " + sensordata.avgValue);
		System.out.println("Min: " + sensordata.minValue);
		System.out.println("Max: " + sensordata.maxValue);
		System.out.println("Current : " + sensordata.curValue);
		System.out.println("totValue : " + sensordata.totValue);
		System.out.println("SampleCount: " + sensordata.sampleCount);
		
		return sensordata;
		
	}
	
	// convert sensorData instance to Json
	// use Gson library and use the toJson() method to convert
	// return the json data that converted from sensorData
	public String sensorDataToJson(SensorData sensorData) {
		String jsonData = null;
		
		if(sensorData != null) {
			Gson gson = new Gson();
			jsonData = gson.toJson(sensorData);
		}
		System.out.println(jsonData);
		
		return jsonData;
	}
	
	// convert json to actuatorData
	// create actuator is null
	// use Gson library and fromJson() method to convert the jsonData to Actuator instance
	// return the actuatorData object
	public ActuatorData jsonToActuatorData(String jsonData) {
		ActuatorData actuatorData = null;
		
		if(jsonData !=null && jsonData.trim().length() > 0) {
			Gson gson = new Gson();
			actuatorData = gson.fromJson(jsonData, ActuatorData.class);
		}
		
		return actuatorData;
	}
	public SensorData jsonToSensorData(String jsonData) {
		SensorData sensorData = null;
		if(jsonData !=null && jsonData.trim().length() > 0) {
			Gson gson = new Gson();
			sensorData = gson.fromJson(jsonData, SensorData.class);
		}
		
		return sensorData;
        
	}
}

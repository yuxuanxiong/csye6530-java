package neu.yuxuanxiong.connecteddevices.project;

import java.util.logging.Level;
import java.util.logging.Logger;


public class MqttSensorPublisher {
	
	private static final Logger _Logger = Logger.getLogger(MqttSensorPublisher.class.getName());
	private static MqttSensorPublisher _App;
	private static String _host = null;
	private static String _pemFileName = "/Users/yuxuanxiong/eclipse-workspace/connected-devices-java/src/main/java/neu/yuxuanxiong/connecteddevices/labs/common/ubidots_cert.pem";
	private static String _userName = "A1E-q4QvqPSBqBpZ2y0tsasiBhCIsIoXou";
	String payload1;
	String payload2;
	static MqttClientConnector _Client = new MqttClientConnector(_host,_userName,_pemFileName);
	
 
	/* Main function
	 * start the app
	 */
	public static void main(String[] args) {
		_App = new MqttSensorPublisher();
	   try {
		   
		   _App.start();
	   } catch (Exception e) {
	    _Logger.log(Level.WARNING, "Failed!! to start _App.", e);
	   }
	  
	 }
	 
	 /*
	  * the start method to establish connection and set topic
	  * generate and publish payload every minute
	  */
	 public void start() {
	  //Create a new MqttClient:_Client and connection
		 _Client.connect();
	  
	  //Set topic 
	  String topic1 = "/v1.6/devices/xyxproject/sensortemp";
	  String topic2 = "/v1.6/devices/xyxproject/sensorhumi";
	
		if(payload1!=null) {
			// publish message to topic1
			_Client.publishMessage(topic1, 0, payload1.substring(1).getBytes());      //publish payload with the topic in Qoslevel 0
		}
		if(payload2!=null) {
			// publish message to topic2
			_Client.publishMessage(topic2, 0, payload2.substring(1).getBytes());      //publish payload with the topic in Qoslevel 0
		}
	    

	  
	  
	 }
	 
}

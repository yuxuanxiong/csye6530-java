package neu.yuxuanxiong.connecteddevices.project;
import java.util.logging.Logger;

public class GatewayManagementApp {
	
	private static final Logger _Logger = 
			Logger.getLogger(GatewayManagementApp.class.getName());
	
	private static GatewayManagementApp _App;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		_App = new GatewayManagementApp();      // new CoapClientTestApp object
		
		try {
			_App.start();					// run start method
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	// private var's
	
	private CoapServerConnector _coapServer;
	
	
	// public methods
	
	/**
	 *  Connect to the CoAP server
	 */
	public void start() {
		_coapServer = new CoapServerConnector(); 			// new CoapClientConnector object
		_coapServer.start();
		
	  
	}


}

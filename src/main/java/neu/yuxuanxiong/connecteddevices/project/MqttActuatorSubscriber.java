package neu.yuxuanxiong.connecteddevices.project;

import java.util.logging.Level;
import java.util.logging.Logger;


public class MqttActuatorSubscriber {
	
	 private static final Logger _Logger = Logger.getLogger(MqttActuatorSubscriber.class.getName());
	 private static MqttActuatorSubscriber _App;
	 private MqttClientConnector _Client;
	 private String _host = null;
	 private String _userName = "A1E-q4QvqPSBqBpZ2y0tsasiBhCIsIoXou";
	 private String _pemFileName = "/Users/yuxuanxiong/eclipse-workspace/connected-devices-java/src/main/java/neu/yuxuanxiong/connecteddevices/labs/common/ubidots_cert.pem";

	/* main method
	 * start the app
	 */
	 public static void main(String[] args) {
	  _App = new MqttActuatorSubscriber();
	  try {
		  
	   _App.start();
	  
	  } catch (Exception e) {
	   _Logger.log(Level.WARNING, "Failed!! to start _App.", e);
	  }
	  
	 }
	 
	 /* start method to establish mqtt secure connection 
	  * set topic
	  * subscribe topic
	  */
	 public void start() {
	  //Create a new MqttClient:_Client and connection
	  _Client = new MqttClientConnector(_host, _userName, _pemFileName);
	  _Client.connect();
	  String topic1 = "/v1.6/devices/xyxproject/actuatetemp";
	  _Client.subscribeToTopic(topic1);

	 }
}

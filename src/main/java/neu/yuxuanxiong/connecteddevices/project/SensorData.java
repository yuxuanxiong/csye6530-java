package neu.yuxuanxiong.connecteddevices.project;

import java.text.SimpleDateFormat;
import java.util.Date;

public class SensorData{
	
	/*
	 *  initialize the timeStamp, name, curValue, avgValue, minValue,
	 *  maxValue, totValue and sampleCount
	 */
	 
	String name = "Temprature";
	String timeStamp = null;
	float  curValue = 0.0f;
	float  avgValue = 0.0f;
	float  minValue = 0.0f;
	float  maxValue = 0.0f;	
	float  totValue = 0.0f;
	int    sampleCount = 0;
	
	/*
	 *  constructor of SensorData class
	 *  set the timeStamp is equal to the datetime now
	 */
     
	public SensorData()
	{
		super();
	    updateTimeStamp();
		
	}
	
	/**
    *This function is to addValue from newVal
    *add sampleCount
    *refresh the time
    *set current value is equal to new value
    *add the new value to total value
    
    if current value is smaller than minimum value, then minValue = curValue
    if current value is larger than maximum value, then maxValue = curValue
    if current value is not zero and samplecount is larger than 0, then calculate average of temperature
    @param param: newVal: the new value that sensorData genetated
    */ 
	public void addValue(float val)
	{
	    updateTimeStamp();
	    ++sampleCount;
	    curValue  = val;
	    totValue += val;
	    if (curValue < minValue) {
	          minValue = curValue;
	}
	    if (curValue > maxValue) {
	          maxValue = curValue;
	}
	    if (totValue != 0 && sampleCount > 0) {
	          avgValue = totValue / sampleCount;
	} }
	
	//get the average value
	public float getAvgValue()
	{
	    return avgValue;
	}
	
	//get max value
	public float getMaxValue()
	{
		
		return maxValue;
    }
	
	//get minimum value
    public float getMinValue()
    {
        return minValue;
    }
    
    //get the name
    public String getName()
    {
        return name;
    }
    
    //get the value
    public float getValue()
    {
        return curValue;
    }
    
    // set the name
    public void setName(String name)
    {
        if (name != null && name.trim().length() > 0) {
              this.name = name;
} }
   
    // update the time
    public void updateTimeStamp()
    {
        timeStamp = new SimpleDateFormat("yyyy.MM.dd HH:mm.ss").format(new Date());
    }
    
    //set the output format of all data 
    public String output() {
		String customStr = this.name + ":\n" + 
							"\tTime:      " + this.timeStamp + "\n" + 
							"\tCurrent:      " + this.curValue + "\n" +
							"\tAverage:      " + this.avgValue + "\n" +
							"\tSamples:      " + this.sampleCount + "\n" +
							"\tMin:      " + this.minValue + "\n" +
							"\tMax:      " + this.maxValue;
		return customStr;
	}
	
}

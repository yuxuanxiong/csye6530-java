package neu.yuxuanxiong.connecteddevices.project;

import java.util.logging.Logger;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.CoapServer;

public class CoapServerConnector {
	
	//static logger to log information on the console
	
	private static final Logger _Logger = Logger.getLogger(CoapServerConnector.class.getName());
	
	// private var's , new CoapServer object
	
	private CoapServer _coapServer;
	
	// constructors
	
	public CoapServerConnector() {
		super();
	}
	
	// public methods of add resource to coap server
	
	public void addResource(CoapResource resource) {
		if(resource != null) {
			_coapServer.add(resource);
		}
 	}
	
	// the start method to create coap server instance and temp handler
	public void start() {
		if(_coapServer == null) {
			_Logger.info("Creating CoAP server instance and 'temp' handler...");
			
			_coapServer = new CoapServer();								// create coap server
			ResourceHandler tempHandler = new ResourceHandler();	// new tempResourceHandler object
			_coapServer.add(tempHandler);								// add tempHandler to coap server
			
		}
		_Logger.info("Starting CoAP server...");
		_coapServer.start();
	}
	
	// the stop method to stop coap server
	public void stop() {
		_Logger.info("Stopping CoAP server...");
		
		_coapServer.stop();
	}

}

package neu.yuxuanxiong.connecteddevices.project;

import java.util.logging.Logger;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.server.resources.CoapExchange;

public class ResourceHandler extends CoapResource {
	
	// initialize variables , instances, new objects 
	
	private static final Logger _Logger = Logger.getLogger(ResourceHandler.class.getName());
	MqttSensorPublisher mqttPub = new MqttSensorPublisher();
	
	// default constructor
	public ResourceHandler() {
		super("temp");
	}
	
	// constructor with params name
	public ResourceHandler(String name) {
		super(name);
	}
	
	// constructor with params name and visible
	public ResourceHandler(String name, boolean visible) {
		super(name,visible);
	}
	
	// method of handle GET request
	public void handleGET(CoapExchange ce) {
		ce.respond(ResponseCode.CREATED, "GET worked!");    // response CREATED code and message of "GET worked"
		_Logger.info("Received GET request from client...");  // log the info that received GET on the console
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.californium.core.CoapResource#handlePOST(org.eclipse.californium.core.server.resources.CoapExchange)
	 * method of handle POST request, get JSON format SensorData from client
	 * transfer JSON into sensorData instance, show on the console
	 * transfer SensorData instance into JSON, show on the console
	 */
	
	public void handlePOST(CoapExchange ce) {
		
		String response = ce.getRequestText();					// get the request text from client
		_Logger.info(response);									// log the request JSON format of sensorData on the console
		ce.respond(ResponseCode.CONTENT, "POST worked!");				// respond the CONTENT response code and JSON format sensorData to client
		
		if(response.contains("T")) {
			mqttPub.payload1 = response;
		}
		if(response.contains("H")) {
			mqttPub.payload2 = response;
		}
		
		mqttPub.start();
		_Logger.info("Received POST request from client...");
	}
	
	// method of handle PUT request
	public void handlePUT(CoapExchange ce) {
		
		ce.respond(ResponseCode.VALID, "PUT worked!");			// respond VALID code and "PUT worked " to client
		
		_Logger.info("Received PUT request from client...");
	}
	
	// method of handle DELETE request
	public void handleDELETE(CoapExchange ce) {
		ce.respond(ResponseCode.DELETED, "DELETE worked!");		// respond DELETED code and "DELETE worked!" to client
		
		_Logger.info("Received DELETE request from client...");
	}
}
